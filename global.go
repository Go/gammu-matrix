package main

import (
	"os"
	"strconv"
)

// Global contains the state from global Gammu variables.
type Global struct {
	SMSMessages  int    // Number of physical messages received.
	DecodedParts int    // Number of decoded message parts.
	PhoneID      string // Value of PhoneID. Useful when running multiple instances.
}

// NewGlobal returns the Gammu global variables.
func NewGlobal() (g *Global, err error) {
	g = &Global{
		PhoneID: os.Getenv("PHONE_ID"),
	}

	g.SMSMessages, err = strconv.Atoi(os.Getenv("SMS_MESSAGES"))
	if err != nil {
		return
	}

	g.DecodedParts, err = strconv.Atoi(os.Getenv("DECODED_PARTS"))
	return
}
