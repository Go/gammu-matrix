package main

import (
	html "html/template"
	"strings"
	text "text/template"
)

var (
	htmlTemplate = `<font color="{{ .Color }}">{{ .Icon }} SMS from <b><a href="tel:{{.Number}}">{{ .Number }}</a></b></font>: {{ .Text }}`
	textTemplate = `{{ .Icon }} SMS from {{ .Number }}: {{ .Text }}`
)

type message struct {
	*Template
	*Message
}

// Template contains the HTML and plain-text templates.
type Template struct {
	Icon, Color  string
	htmlTemplate *html.Template
	textTemplate *text.Template
}

// NewTemplate initializes a Template.
func NewTemplate(icon, color string) (t *Template) {
	t = &Template{Icon: icon, Color: color}
	t.htmlTemplate = html.Must(html.New("").Parse(htmlTemplate))
	t.textTemplate = text.Must(text.New("").Parse(textTemplate))
	return
}

// RenderMessage renders a Message in plain-text and HTML.
func (t *Template) RenderMessage(m *Message) (text, html string, err error) {
	var textBuf, htmlBuf strings.Builder

	msg := &message{Template: t, Message: m}
	err = t.textTemplate.Execute(&textBuf, msg)
	if err != nil {
		return "", "", err
	}

	err = t.htmlTemplate.Execute(&htmlBuf, msg)
	if err != nil {
		return "", "", err
	}

	return textBuf.String(), htmlBuf.String(), nil
}
