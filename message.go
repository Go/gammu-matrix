package main

import (
	"fmt"
	"os"
)

// Message contains a Gammu (SMS) message.
type Message struct {
	Class     string // Class of message.
	Number    string // Sender number.
	Text      string // Message text. Text is not available for 8-bit binary messages.
	Reference string // Message Reference. If delivery status received, this variable contains TPMR of original message
}

// NewMessage creates a new Message from Gammu's environment variables.
func NewMessage(i int) *Message {
	prefix := fmt.Sprintf("SMS_%d_", i)
	return &Message{
		Class:     os.Getenv(prefix + "CLASS"),
		Number:    os.Getenv(prefix + "NUMBER"),
		Text:      os.Getenv(prefix + "TEXT"),
		Reference: os.Getenv(prefix + "REFERENCE"),
	}
}
