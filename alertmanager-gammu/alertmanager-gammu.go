package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"

	"github.com/silkeh/alertmanager_matrix/alertmanager"
	"github.com/silkeh/alertmanager_matrix/bot"
)

func handler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	// Get number from request
	numbers, ok := r.URL.Query()["number"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Validate numbers
	for _, n := range numbers {
		if !strings.HasPrefix(n, "+") && !strings.HasPrefix(n, "00") {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	// Parse the message
	data := new(alertmanager.Message)
	if err := json.NewDecoder(r.Body).Decode(data); err != nil {
		log.Printf("Error parsing message: %s", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Create readable messages for Matrix
	plain, _ := bot.FormatAlerts(data.Alerts, false)
	log.Printf("Sending message to %v: %s", numbers, plain)

	// Send messages
	for _, n := range numbers {
		err := sendMessage(n, plain)
		if err != nil {
			log.Printf("Error")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

func sendMessage(number, text string) error {
	var out bytes.Buffer

	cmd := exec.Command(gammuPath, "TEXT", number, "-unicode", "-text", text)
	cmd.Stdout = &out
	cmd.Stderr = &out

	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("error running %q: %w. Output: %s", gammuPath, err, out.String())
	}

	return nil
}

func setStringFromEnv(target *string, env string) {
	if str := os.Getenv(env); str != "" {
		*target = str
	}
}

func setMapFromJSONFile(m *map[string]string, fileName string) {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal("Unable to open JSON file: ", err)
	}
	err = json.NewDecoder(file).Decode(m)
	if err != nil {
		log.Fatal("Unable to parse JSON file: ", err)
	}
}

var gammuPath string

func main() {
	var addr, iconFile string

	flag.StringVar(&addr, "addr", ":4052", "Address to listen on (ADDR)")
	flag.StringVar(&iconFile, "icon-file", "", "JSON file with icons for message types (ICON_FILE)")
	flag.StringVar(&gammuPath, "gammu-path", "gammu-smsd-inject", "Path/binary of `gammu-smsd-inject`")
	flag.Parse()

	// Set variables from the environment
	setStringFromEnv(&addr, "ADDR")
	setStringFromEnv(&iconFile, "ICON_FILE")
	setStringFromEnv(&iconFile, "GAMMU_PATH")

	// Load mappings from files
	if iconFile != "" {
		setMapFromJSONFile(&bot.AlertIcons, iconFile)
	}

	// Create/start HTTP server
	r := http.NewServeMux()
	r.HandleFunc("/sms", handler)
	log.Print("Listening on ", addr)
	log.Fatal(http.ListenAndServe(addr, r))
}
