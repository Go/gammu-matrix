package main

import (
	"encoding/json"
	"errors"
	"io/fs"
	"os"
)

// Config contains the application configuration.
type Config struct {
	HomeserverURL string `json:"homeserver_url"`
	UserID        string `json:"user_id"`
	AccessToken   string `json:"access_token"`
	RoomID        string `json:"room_id"`
	MessageType   string `json:"message_type"`
	MessageIcon   string `json:"message_icon"`
	MessageColor  string `json:"message_color"`
}

// NewConfig opens configuration files. The first existing file is opened.
func NewConfig(paths ...string) (*Config, error) {
	file, err := openFile(paths)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	config := new(Config)
	err = json.NewDecoder(file).Decode(config)
	if err != nil {
		return nil, err
	}

	if config.MessageType == "" {
		config.MessageType = "m.notice"
	}

	if config.MessageIcon == "" {
		config.MessageIcon = "💬"
	}

	if config.MessageColor == "" {
		config.MessageColor = "blue"
	}

	return config, err
}

func openFile(files []string) (file *os.File, err error) {
	for _, path := range files {
		if file, err = os.Open(path); err == nil || !errors.Is(err, fs.ErrNotExist) {
			return
		}
	}
	return
}
