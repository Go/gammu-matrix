package main

import (
	"fmt"
	"os"
)

// Part contains a Gammu decoded part.
type Part struct {
	Text       string // Decoded long message text.
	MMSSender  string // Sender of MMS indication message.
	MMSTitle   string // title of MMS indication message.
	MMSAddress string // Address (URL) of MMS from MMS indication message.
	MMSSize    string // Size of MMS as specified in MMS indication message.

}

// NewPart creates a new Part from Gammu's environment variables.
func NewPart(i int) *Part {
	prefix := fmt.Sprintf("DECODED_%d_", i)
	return &Part{
		Text:       os.Getenv(prefix + "TEXT"),
		MMSSender:  os.Getenv(prefix + "MMS_SENDER"),
		MMSTitle:   os.Getenv(prefix + "MMS_TITLE"),
		MMSAddress: os.Getenv(prefix + "MMS_ADDRESS"),
		MMSSize:    os.Getenv(prefix + "MMS_SIZE"),
	}
}
