package main

import (
	"log"

	"github.com/silkeh/alertmanager_matrix/matrix"
)

func main() {
	config, err := NewConfig("config.json", "/etc/gammu-matrix.json")
	if err != nil {
		log.Fatalf("Error reading configuration: %s", err)
	}

	client, err := matrix.NewClient(config.HomeserverURL, config.UserID, config.AccessToken, config.MessageType)
	if err != nil {
		log.Fatalf("Error initializing Matrix client: %s", err)
	}

	g, err := NewGlobal()
	if err != nil {
		log.Fatalf("Error reading global variables: %s", err)
	}

	template := NewTemplate(config.MessageIcon, config.MessageColor)
	room := client.NewRoom(config.RoomID)
	for i := 1; i <= g.SMSMessages; i++ {
		m := NewMessage(i)
		text, html, err := template.RenderMessage(m)
		if err != nil {
			log.Printf("Error rendering message: %s", err)
			continue
		}

		_, err = room.SendHTML(text, html)
		if err != nil {
			log.Printf("Error sending Matrix message: %s", err)
		}
	}

	for i := 1; i <= g.DecodedParts; i++ {
		p := NewPart(i)
		log.Printf("Part %d: %#v", i, p)
	}
}
